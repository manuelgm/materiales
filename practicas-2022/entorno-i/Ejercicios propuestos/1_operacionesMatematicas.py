numero1 = 7
numero2 = 3

# Realiza operaciones matemáticas
suma = numero1 + numero2
resta = numero1 - numero2
multiplicacion = numero1 * numero2
division = numero1 / numero2
exponente = numero1 ** numero2
raiz_cuadrada_numero1 = numero1 ** 0.5

# Muestra los resultados
print("Suma:", suma)
print("Resta:", resta)
print("Multiplicación:", multiplicacion)
print("División:", division)
print("Exponente:", exponente)
print("Raíz cuadrada de", numero1, ":", raiz_cuadrada_numero1)
