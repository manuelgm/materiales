frase = input("Ingresa una frase: ")
frase_modificada = ""

for caracter in frase:
    ascii_valor = ord(caracter)
    if ascii_valor in [97, 101, 105, 111, 117, 225, 233, 237, 243, 250]:
        frase_modificada += chr(ascii_valor - 32)
    else:
        frase_modificada += caracter

print("Frase con vocales en mayúsculas:", frase_modificada)
