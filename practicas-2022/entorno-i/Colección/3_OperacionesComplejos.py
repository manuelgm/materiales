real1 = float(input("Ingresa la parte real del primer número complejo: "))
imaginario1 = float(input("Ingresa la parte imaginaria del primer número complejo: "))
real2 = float(input("Ingresa la parte real del segundo número complejo: "))
imaginario2 = float(input("Ingresa la parte imaginaria del segundo número complejo: "))

num_complejo1 = complex(real1, imaginario1)
num_complejo2 = complex(real2, imaginario2)

suma = num_complejo1 + num_complejo2
producto = num_complejo1 * num_complejo2

print("Primer número complejo:", num_complejo1)
print("Segundo número complejo:", num_complejo2)
print("Resultado de la suma:", suma)
print("Resultado de la multiplicación:", producto)

