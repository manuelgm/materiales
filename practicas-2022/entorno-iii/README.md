# El entorno de programación III

## GitLab

Actividades:

* [GitLab.com](https://gitlab.com) y el [GitLab de la ETSIT](https://gitlab.etsit.urjc.es)

* Autenticación en el GitLab de la ETSIT.
  * Observa que hay dos pestañas en el panel de autenticación.
  * Puedes autenticarte con tu cuenta de los laboratorios de la ETSIT, o con tu cuenta de alumno de la Universidad.
  * Es preferible que utilices la segunda, pero utilices la que utilices, es mejor que utilices siempre la misma.

* Proyecto (repositorio) GitLab.
  * [Repo de la asignatura en GitLab.com](https://gitlab.com/cursoprogram/cursoprogram.gitlab.io)
  * [Repo de recursos de la asignatura en ETSIT GitLab](https://gitlab.etsit.urjc.es/cursoprogram/materiales/)

* Proyectos de un grupo o usuario.
  * [Grupo de proyectos de la asignatura en ETSIT GitLab](https://gitlab.etsit.urjc.es/cursoprogram)
  * [Usuario jesus.gonzalez.barahona en ETSIT GitLAb](https://gitlab.etsit.urjc.es/jesus.gonzalez.barahona)
  * [Usuario jgbarah en GitLab.com](https://gitlab.com/jgbarah)

* Edición de ficheros
  * El botón de editar un fichero
  * El IDE de GitLab

**Ejercicio:** "Creación de un repositorio":

* Crea un repositorio en ETSIT GitLAb que quede como repositorio de tu usuario.
* Asegúrate de que el repositorio es público, o interno.

**Ejercicio:** "Bifurcación de un repositorio":

Bifurca (haz un fork) del repositorio [Bifurca Repositior](https://gitlab.etsit.urjc.es/cursoprogram/bifurca-repositorio/).
El repositorio resultante debe quedar como un repositorio de tu usuario.
Una vez que tengas ese nuevo repositorio, utilizando la interfaz web de GitLab, modifica el fichero README.md que hay en él para añadir, después del enunciado, el texto "Este es el repositorio de <nombre>", donde <nombre> es tu nombre.
Asegúrate de que el repositorio es público (visible para todos los visitantes) o interno (visible para los usuarios que se hayan autenticado).

**Ejercicio a entregar:** "Cálculo de números primos"

[Enunciado](../ejercicios/README.md#primos), incluyendo repositorio plantilla y fecha de entrega.

## Git

* Clonado de repositories

```commandline
git clone <url>
```

* Consulta de remotos

```commandline
git remote -v
```

* Consulta del histórico

```commandline
git log
```

* Consulta de estado

```commandline
git status
```

* Añadir ficheros nuevos

```commandline
git add <fichero>
```

* Realizar un commit (atención al punto al final de la línea)

```commandline
git commit -m "Mensaje del commit" .
```

* Subir un commit a un remoto

```commandline
git push
```

* Recibir commits de un remoto

```commandline
git fetch
git rebase origin/master
```

o

```commandline
git fetch
git merge
```

Nosotros recomendamos (para las prácticas de esta asignatura) usar la primera opción, pero si quieres conocer más detalles sobre las diferencias, lee [Git Rebase vs. Git Merge: Which Is Better?](https://www.perforce.com/blog/vcs/git-rebase-vs-git-merge-which-better)

* Flujo normal de prácticas: Normalmente tendremos dos repositorios git para una práctica:
  * Uno remoto (en ETSIT GitLab), que normalmente habrá empezado como una bifurcación (fork) del repositorio plantilla (uno para cada práctica, mencionado en el enunciado).
  * Otro será el repositorio "local", en el ordenador de trabajo, que se obtendrá clonando el remoto (`git clone`).
  * Cada vez que se hagan cambios en el repositorio local que compongan una nueva versión, se hará `git commit` (quizás previamente haga falta `git add`).
  * Cuando se quiera (se aconseja cada vez que se haga un commit) se enviarán los commits al repositorio remoto, con `git push`.
  * En el remoto, se puede comprobar que los commits se han enviado bien viendo la historia vía la interfaz de GitLab. 
  * Si se hubiera hecho algún cambio en el repositorio remoto (por ejemplo, usando el IDE de GitLab), habrá que recibir los commits correspondientes en el repositorio local, por ejemplo con `git fetch` y `git rebase`.
  * En el repositorio local, se puede comprobar qu elos commits se han recibido bien  viendo la historia vía `git log`.
  * Se puede ver qué ficheros están "bajo control de git" y por tanto en los commits viendo el estado del repositorio (`git status`).

* Git desde PyCharm. Todas las opciones relevantes las tenemos en el menú "Git".

* Flujo de prácticas con tres repos: En algunos casos, se querrá tener además del repositorio remoto en ETSIT GitLab y el repositorio local en el laboratorio, otro repositorio local en otro ordenador (el ordenador de casa, por ejemplo).
  * En este caso es importante asegurarse de que todos los cambios se suben como commits al repo de GitLab antes de empezar a trabajar en el otro repo "local".
  * El flujo de trabajo en un repo local comenzará por tanto siempre recibiendo los cambios (commits) que pueda haber en el repo en GitLab, haciendo localmente los cambios que se quiera, y al terminar enviándolos al repo remoto en GitLab como uno o varios commits. De esta forma, el repositorio en GitLab siempre servirá para sincronizar cualquiera de los repositorios locales.

**Ejercicio:** "Clonación y actualización de repositorio remoto"

Clona a un directorio local el repositorio que bifurcaste (hiciste fork) del repositorio [Bifurca Repositorio](https://gitlab.etsit.urjc.es/cursoprogram/bifurca-repositorio/) en un ejercciio anterior (ejercicio "Bifurcación de un repositorio"). Una vez lo hayas clonado, modifica el fichero `README.md` que hay en él, realiza una nueva versión (commit), y súbela al repostorio del que clonaste.

A continuación, crea un nuevo fichero, realiza una nueva versión que lo incluya, y súbela también al repositorio del que clonaste.

Haz lo anterior primero desde línea de comandos, y luego desde PyCharm.

**Ejercicio a entregar:** "Triángulo de números"

[Enunciado](../ejercicios/README.md#triangulo), incluyendo repositorio plantilla y fecha de entrega.

Referencias:

* [Pro Git](https://git-scm.com/book/en/v2). Libro sobre git, que cubre desde el uso básico hasta varios detalles avanzados, incluyendo el uso con GitHub (y GitLab).

* [Oh My Git!](https://blinry.itch.io/oh-my-git). Juego de cartas para aprender detalles de git, incluyendo cómo entender el grafo de versiones.
