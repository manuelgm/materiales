# Proyecto final

## Frikiminutos Python:

* [Descarga de audios de YouTube](../../frikiminutos-2022/README.md#youtube).

## GitLab

Actividades:

*

**Ejercicio:** "Sonido de una frecuencia":

Escribe un programa que, usando el módulo [sounddevice](https://python-sounddevice.readthedocs.io) (que habrá que instalar en un entorno virtual), genere el sonido de una cierta frecuencia, y lo haga sonar en el altavoz, primero en el izquierdo y luego en el derecho. Para ello, tendrás primero que crear un vector (array) de enteros de 16 bits (usando el tipo de objetos [array.array](https://docs.python.org/3/library/array.html)). Cada elemento de este vector tendrá una muestra de sonido. A continuación, genera las muestras de sonido utilizando la función `math.sin`.

Por último, haz sonar estas muetras primero por el canal izquierdo, luego por el derecho, y luego por los dos. Para esto, utliza la función `sounddevice.play`, con un código similar al siguiente:

```python
    sounddevice.play(sound, samplerate=samples_second, mapping=[1])
    sounddevice.wait()
```

Solución: [play_freq.py](play_freq.py)

**Ejercicio:** "Grabación de un fichero de audio":

Escribe un programa que, usando el módulo [sounddevice](https://python-sounddevice.readthedocs.io) (que habrá que instalar en un entorno virtual), grabe el sonido recogido por el micrófono del ordenador en un fichero en formato WAV. Concretamente, puedes usar la función [sounddevice.rec](https://python-sounddevice.readthedocs.io/en/0.4.5/api/convenience-functions.html#sounddevice.rec).

Para escribir el sonido en un fichero, utiliza el módulo [soundfile](https://pysoundfile.readthedocs.io), y en concreto la función [soundfile.write](https://pysoundfile.readthedocs.io/en/latest/#soundfile.write)

El programa recibirá como argumentos en la línea de comandos el nombre del fichero donde se va a grabar el sonido, y el número de segundos durante el que se realizará la grabación:

```commandline
python3 sound_record.py <filename> <seconds>
```

En algunos casos, tendrás que elegir el dispositivo de entrada (micrófono) o el de salida (altavoz), por ejemplo si el que esté configurado por defecto no funciona. El módulo `sounddevice` proporciona una función para obtener la lista de dispositivos disponibles: [query_device](https://python-sounddevice.readthedocs.io/en/0.4.5/api/checking-hardware.html#sounddevice.query_devices). Esta función se ejecuta automáticamente cuando ejecutas desde un terminal el siguiente comando (en un entorno virtual donde esté instalado el módulo `sounddevice`):

```commandline
 python3 -m sounddevice
```

Para elegir el dispositivo, en la llamada correspondiente (`rec`, `play`, etc.) utiliza el parámetro `device`. Por ejemplo, para utilizar el dispositivo `default` al grabar se puede usar:

```python
sound = sounddevice.rec(timespan * samples_second,
                        samplerate=samples_second, channels=2, dtype='int16', device='default')
```

Solución: [sound_record.py](sound_record.py)

**Ejercicio:** "Reproduce sonido":

Escribe un programa que, usando el módulo [sounddevice](https://python-sounddevice.readthedocs.io) reproduzca el sonido de un fichero WAV en los altavoces del ordenador. Para ello utliza la función [sounddevice.play](https://python-sounddevice.readthedocs.io/en/0.4.5/api/convenience-functions.html#sounddevice.play).

Para leer el sonido de un fichero, utiliza el módulo [soundfile](https://pysoundfile.readthedocs.io), y en concreto la función [soundfile.read](https://pysoundfile.readthedocs.io/en/latest/#soundfile.read)

El programa recibirá como argumentos en la línea de comandos el nombre del fichero con sonido que se va a reproducir:

```commandline
python3 sound_record.py <filename>
```

Solución: [sound_play.py](sound_play.py)

**Ejercicio:** "Visualiza la amplitud del sonido":

Escribe un programa que, usando el módulo [sounddevice](https://python-sounddevice.readthedocs.io) reproduzca el sonido de un fichero WAV en los altavoces del ordenador, a la vez que muestra en pantalla una visualización de la amplitud media del sonido durante un corto periodo de tiempo (por ejemplo, 200 ms).

El programa recibirá como argumentos en la línea de comandos el nombre del fichero con sonido que se va a reproducir:

```commandline
python3 sound_bars.py <filename>
```

Una posible ejecución sería:

```commandline
$ python play_bars.py recording.wav 
Reading: recording.wav
                                 |                                 
                                 |                                 
                                 |                                 
                               **|**                               
                                *|*                                
                                 |                                 
                                 |                                 
                                 |                                 
                                *|*                                
                                 |                                 
                                 |                                 
                                 |                                 
                                 |                                 
                                *|*                                
                                 |                                 
```

Solución: [sound_bars.py](sound_bars.py)

**Ejercicio a entregar:** "Proyecto final"

[Enunciado](../ejercicios/README.md#final), incluyendo repositorio plantilla y fecha de entrega.

Referencias:

* [sounddevice module](https://python-sounddevice.readthedocs.io)
* [soundfile module](https://pysoundfile.readthedocs.io)

**Ejercicio a entregar:** "Proyecto final (junio)"

[Enunciado](../ejercicios/README.md#final2), incluyendo repositorio plantilla y fecha de entrega.

