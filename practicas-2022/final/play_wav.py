#!/usr/bin/env python3

import os
import sys

import sounddevice
import soundfile

def main():

    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} <file_path>")
        exit(0)

    filename = sys.argv[1]

    file_stats = os.stat(filename)
    print(f' File size: {file_stats.st_size}')

    print(f"Reading: {filename}")
    data, fs = soundfile.read(filename, dtype='int16')

    print("Now playing")
    sounddevice.play(data, fs)
    sounddevice.wait()


if __name__ == '__main__':
    main()