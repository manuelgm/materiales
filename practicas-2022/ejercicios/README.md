# Ejercicios

## Introducción

### <a name="segundos">Días, horas, minutos y segundos</a>

Escribe un programa en Python3 que lea (de teclado) una cantidad de segundos y los convierta en días, horas, minutos y segundos. El programa, al arrancar, mostrará un mensaje en en pantalla:

```
Número de segundos:
```

y esperará que el usuario escriba un número entero, y la tecla ·"Enter". A continuación escribirá:

```
xxx segundos son ddd día(s), hhh hora(s), mmm minuto(s) y sss segundo(s)
```

Por ejemplo, si se introduce el número 90061, el programa escribirá:

```
90061 segundos son 1 día(s), 1 hora(s), 1 minuto(s) y 1 segundo(s)
```

En la cabecera del programa, pon un comentario que indique qué hace el programa, y el nombre de quien lo ha escrito.

* [Solución](intro-segundos/segundos.py)

* [Solución con funciones y tests](intro-segundos-2/segundos.py) (para probar tests)

* [Solución con funciones y tipos](intro-segundos-3/segundos.py) (para probar MyPy)

## Estructuras de control

### <a name="impares">Suma de números impares</a>

Construir un programa que realice la suma de todos los números impares comprendidos entre dos números enteros no negativos introducidos por teclado por el usuario.

Al arrancar, el programa escribirá: `Dame un número entero no negativo: `. Cuando el usuario escriba el número, el programa escribirá: `Dame otro: `. A continuación, el programa mostrará por pantalla la suma de los números impares comprendidos entre esos dos números, incluidos cualquiera de ellos si es un número impar.

Llama al programa `impares.py`.

* **Fecha de entrega:** 12 de octubre de 2022, 23:59
* Entrega en foro de ejercicios
* [Solución](control-impares/impares.py)

### <a name="primos">Cálculo de números primos</a>

* **Fecha de entrega:** 19 de octubre de 2022, 23:59
* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/primos

Construir un programa que solicite un número entero no negativo al usuario, escribiendo el mensaje "Dame un número entero no negativo: ", y que como respuesta escriba en una línea el mensaje "Números primos iguales o menores: ", y a continuaciòn, en la misma línea, la lista de los números primos menores o iguales que él, separados por espacios. Para calcular los números primos, utiliza la definición: "un número entero es primo si sólo es divisible por 1 y por él mismo".

Un ejemplo de ejecución podría ser:

```commandline
Dame un número entero no negativo: 11
Números primos iguales o menores: 1 2 3 5 7 11
```

Otro podría ser:

```commandline
Dame un número entero no negativo: 12
Números primos iguales o menores: 1 2 3 5 7 11
```

Llama al programa `primos.py`

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

## Divide y vencerás

### <a name="triangulo">Triángulo de números</a>

* **Fecha de entrega:** 26 de octubre de 2022, 23:59
* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/triangulo

Escribir un programa que acepte como argumento (en línea de comandos) un número, y muestre un triángulo de números de la forma siguiente: primero una línea con un 1, luego una línea con dos 2, luego una línea con tres 3, y así hasta una línea que tenga número veces número.

Por ejemplo, si el número que se pasó como argumento es 4, de esta forma:

```commandline
python3 triangulo.py 4
```

se mostrará:

```
1
22
333
4444
```

El programa debe llamarse `triangulo.py`, y debe usar el esquema que muestra el fichero `esquema.py` que puedes encontrar en el repositorio de plantilla de esta práctica. Como verás, en ese esquema, todo el código va en la función `main`, y el "programa principal" (el código que va después de `if __name__ ...`) servirá simplemente para llamar a `main`, que hará todo el trabajo, llamando a su vez a las funciones que necesite. Para escribir tu programa te recomendamos que copies el contenido de `esquema.py` en `triangulo.py`, y completes luego `triangulo.py` hasta que funcione como se especifica en este enunciado.

El programa deberá tener, además de `main`, al menos dos funciones:

* `line`, que aceptara como parámetro un número entero, y devolverá una cadena de texto (string) con la línea correspondiente a ese número. Por ejemplo, si el parámetro es el número `3`, devolverá la cadena `333` (sin fin de línea al final).
* `triangle`, que aceptará como parámetro un número entero, y devolverá una cadena de texto (string) con las líneas correspondientes a ese número (incluyendo fines de línea al final de cada cadena). Por ejemplo, si el parámetro es el número `3`, devolverá la cadena `1\n22\n333\n` (ver más adelante el significado de `\n`). Si el parámetro es mayor que 9, levantará la excepción 'ValueError'

La función `main` leerá el valor del argumento pasado en la línea de comandos, y llamará a la función `triangle` con el valor de ese argumento (como entero). Si la función levanta la excepción `ValueError`, volverá a llamar a `triangle` con el valor 9. Luego, mostrará el texto que le haya devuelto `triangle` en pantalla.

Para poder escribir correctamente estas funciones, ten en cuenta lo siguiente:

* El operador `+` se puede utilizar con las cadenas de texto, y el resultado es que concatenará las cadenas a las que se aplique el operador. Por ejemplo, la expresión `"A" + "B"` tiene como resultado la cadena (string) `"AB"`, o la expresión `"ABC" + "DE"` tiene como resultado la cadena `"ABCDE"`.
* El operador `*`, cuando se aplica a una cadena y un número entero, produce como resultado una cadena igual a la origina, repetida ese número de veces. Por ejemplo, `"ABC" * 4` produce como resultado `"ABCABCABCABC"`. 
* El carácter `\n` se interpreta en Python como el fin de línea. Así, la cadena `"ABC\n"` se interpeta como la cadena `ABC` seguida de un fin de línea.
* Al mostrar con `print` una cadena que incluya fines de línea, cada fin de línea hará que en pantalla aparezca una nueva línea. Así, la cadena `print("A\nBB\n")` mostrará en pantalla:

```
A
BB
```

Por otro lado, para leer un argumento de la línea de comandos, puedes utilizar la variable `sys.argv`, que "prerellena" el intérrpete de Python. Por ahora basta con que sepas que el valor primer argumento que recibe un programa puede consultarse como `sys.argv[1]`, y será siempte una cadena (string). Más adelante verás que `sys.argv` es una lista, y podrás entender mejor esta sintaxis. Para poder consultar `sys.argv` hay que importar primero el módulo `sys`, con una línea como la siguiente:

```python
import sys
```

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`. El proceso resumido para hacerlo es el siguiente:

* Cuando hayas creado el repo bifurcado (fork) del repositorio plantilla, consigue su URL HTTPS de clonado, usando el botón "Clone" del repositorio en ETSIT GitLab.
* Utiliza, en tu ordenador, esa url para clonar el repositorio en un directorio local. Si `<url>` es la URL que has obtenido de tu repositorio en GitLab:

```commandline
git clone <url>
```

* Eso te producirá un directorio que tendrá el mismo nombre que el final de la URL del repositorio, en este caso, `triangulo`.

* Abre ese directorio como proyecto en GitLab, y modifica el código de `triangulo.py` hasta que el programa funcione como debe.

* Crea al menos un commit (versión) con PyCharm o con git en línea de comandos. Si lo haces desde la línea de comandos, tendrás que ejecutar (ojo con el punto al final, y con las comillas al principio y al final del comentario que quieras escribir):

```commandline
git add triangulo.py
git commit -m "Comentario de lo que hace el commit" .
```

Los comandos `git` los tendrás que ejecutar desde el directorio donde tienes tu repositiro git local, así que primero tendrás que haberte "cambiado" a él (`cd triangulo` o algo parecido), o ejecutarlo desde el terminal de PyCharm para este proyecto.

* Sube el commit con PyCharm o con git en línea de comando a tu repositirio en ETSIT GitLab. Si lo haces desde la línea de comando:

```commandline
git push
```

Al terminar, comprueba en ETSIT GitLab que el fichero `triangulo.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en ETSIT GitLab como "público" o "interno", para que podamos recoger la práctica.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_triangulo.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_triangulo.py
```

## Listas y tuplas

### <a name="ordenpalabras">Ordenación de palabras</a>

* **Fecha de entrega:** 2 de noviembre de 2022, 23:59
* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/ordenpalabras

Realiza un programa, que se llame `sortwords.py`, que ordene alfabéticamente una lista de palabras que se le proporcionen como argumento en la línea de comandos, utilizando el método de selección. Por ejemplo:

```commandline
python3 sortwords.py Hola adios Voy vengo
adios Hola vengo Voy
```

La lista de palabras se ordenará sin tener en cuenta mayúsculas o minúsculas (esto es, tanto la `a` como la `A` irán antes de la `b` o la `B`).  Utiliza una función para decidir si una cadena de caracteres es menor (está antes alfabéticamente) que otra. Utiliza ordenación por selección para ordenar las cadenas de caracteres.

El programa debe usar el esquema que muestra el fichero `plantilla.py` que puedes encontrar en el repositorio de plantilla de esta práctica. En él hay varias funciones:

* El programa principal está en la función `main`, como ya hemos hecho en otras prácticas.

* La ordenación la realiza una función `sort`, que recibe como parámetro la lista de palabras a ordenar, y produce como resultado la lista de palabras ordenada.

* A su vez, `sort` utiliza al función `get_lower`, que recibe como parámetros la lista de palabras a ordenar y la posición pivote, y devuelve la posición de la palabra más baja (anterior según el orden alfabético) desde la posición pivote hacia la derecha (incluyendo la posición pivote).

* A su vez, `get_lower` utiliza la función `is_lower`, que compara dos palabras y devuelve un booleano indicando si la primera palabra es menor (anterior alfabéticamente) que la segunda, o no. Además, `is_lower` se encarga, al comparar caracteres, de pasarlos primero a minúsculas, para que la comparación sea independiente de si la letra es minúscula o mayúscula. Para hacer esto, puede usarse la función `lower`. Por ejemplo, para obtener una versión en minúscula de la letra `A`, puede usarse `A.lower()`. Si las variables `letra` y `letramin` son de tipo `string`, puede usarse `letramin = letra.lower()` para tener en `letramin` la letra que haya en `letra`, pero pasada a minúscula si era mayúscula. 

* La función `main` también usará la función `show`, que recibe como parámetro una lista de palabras, y la muestra en pantalla en el formato que se ve más arriba en el ejemplo.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`, siguiendo el mismo proceso que en el ejercicio de entrega anterior (en su enunciado tienes detalles sobre el proceso).

Al terminar, comprueba en ETSIT GitLab que el fichero `sortwords.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en ETSIT GitLab como "público" o "interno", para que podamos recoger la práctica.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_sortwords.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_sortwords.py
```

### <a name="ordenpalabras_ins">Ordenación de palabras mediante inserción</a>

* **Fecha de entrega:** 9 de noviembre de 2022, 23:59
* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/ordenpalabras_ins


Realiza un programa, que se llame `sortwords.py`, que ordene alfabéticamente una lista de palabras que se le proporcionen como argumento en la línea de comandos, igual que en el ejercicio anterior, pero usando el método de inserción. Por ejemplo:

```commandline
python3 sortwords.py Hola adios Voy vengo
adios Hola vengo Voy
```

La lista de palabras se ordenará sin tener en cuenta mayúsculas o minúsculas (esto es, tanto la `a` como la `A` irán antes de la `b` o la `B`).  Utiliza una función para decidir si una cadena de caracteres es menor (está antes alfabéticamente) que otra. Utiliza ordenación por selección para ordenar las cadenas de caracteres.

El programa debe usar el esquema que muestra el fichero `plantilla.py` que puedes encontrar en el repositorio de plantilla de esta práctica. En él hay varias funciones:

* El programa principal está en la función `main`, como ya hemos hecho en otras prácticas.

* La ordenación la realiza una función `sort`, que recibe como parámetro la lista de palabras a ordenar, y produce como resultado la lista de palabras ordenada.

* A su vez, `sort` utiliza al función `sort_pivot`, que recibe como parámetros la lista de palabras a ordenar y la posición pivote. Esta función se encargará de colocar el elmento que esté en la posición pivote lo más a la izquierda que le toque, comparando con las palabras que estén a su izquierda hasta que encuentre una más pequeña (anterior en el diccionario).

* A su vez, `sort_pivot` utiliza la función `is_lower`, que compara dos palabras y devuelve un booleano indicando si la primera palabra es menor (anterior alfabéticamente) que la segunda, o no. Además, `is_lower` se encarga, al comparar caracteres, de pasarlos primero a minúsculas, para que la comparación sea independiente de si la letra es minúscula o mayúscula. Para hacer esto, puede usarse la función `lower`. Por ejemplo, para obtener una versión en minúscula de la letra `A`, puede usarse `A.lower()`. Si las variables `letra` y `letramin` son de tipo `string`, puede usarse `letramin = letra.lower()` para tener en `letramin` la letra que haya en `letra`, pero pasada a minúscula si era mayúscula. 

* La función `main` también usará la función `show`, que recibe como parámetro una lista de palabras, y la muestra en pantalla en el formato que se ve más arriba en el ejemplo.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`, siguiendo el mismo proceso que en el ejercicio de entrega anterior (en su enunciado tienes detalles sobre el proceso).

Al terminar, comprueba en ETSIT GitLab que el fichero `sortwords.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en ETSIT GitLab como "público" o "interno", para que podamos recoger la práctica.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_sortwords.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_sortwords.py
```

En el repositorio plantilla hay además un fichero `.gitlab-ci.yml`, que hará que el sistema CI (Continuous Integration) de GitLab ejecute automáticamente los tests cada vez que se suba un nuevo commit. Puees ver el resultado en la opción correspondiente del menú vertical de GitLab (cuando estés viendo tu repositorio).

### <a name="aproximacion">Árboles por aproximación</a>

* **Fecha de entrega:** 16 de noviembre de 2022, 23:59
* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/aproximacion

Vamos a calcular el número óptimo de árboles, de forma que produzcan la mayor cantidad
de fruta posible. Para ello, consideremos un campo con árboles frutales. Si hay `base_tree`
árboles, producen `fruit_per_tree` kilos de fruta cada uno.
Por cada árbol más que se planta, la producción de cada árbol en el campo
se reduce en `reduction` kilos de fruta.
Calcular el número óptimo de árboles entre dos números dados (`min` y `max`).

Realiza un programa, que se llame `aprox.py` que resuelva este problema. Para ello,
utiliza la plantilla `plantilla.py` que encontrarás en el repositorio de plantilla,
y ten en cuenta el programa `aprox_simple.py` que es una versión simplificada de lo
que tienes que hacer.

El programa `aprox.py` se llamará de la siguiente forma:

```commandline
python3 aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>
```

Por ejemplo, si se le llama así:

```commandline
python3 aprox.py 25 400 10 27 34 
```

producirá como resultado:

```commandline
27 10260
28 10360
29 10440
30 10500
31 10540
32 10560
33 10560
34 10540
Best production: 10560, for 32 trees
```

Además, el programa comprobará que ha sido llamado con el número adecuado de argumentos,
y si no fuera así el programa terminará y mostrará el mensaje:

```commandline
Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>
```

También comprobará que todos los argumentos que se le pasen son números enteros.
Si alguno no lo es el programa terminará y mostrará el mensaje:

```commandline
All arguments must be integers
```

Ten en cuenta que, según se indica en la plantilla, el programa principal no
hará nada salvo llamar a la función `main`. Esta llamará primero a 
'read_arguments' para obtener los argumentos de la línea de comandos, ya
como números enteros, y luego a `compute_all` para obtener una lista donde
cada elemento será una tupla (número de árboles, producción). `main` terminará
imprimiendo los valores de esa lista, calculando la mejor producción, y
mostrando el mensaje final con la mejor producción.

`compute_all`, por lo tanto, recibirá como argumento el número de árboles
mínimo y máximo, y calculará las producciones para cualquier número de árboles
entre ese mínimo y ese máximo, devolviendo la lista de tuplas que se menciona
anteriormente. Para hacer su trabajo, `compute_all` llamará a `compute_trees`
cuando le haga falta calcular la producción para un número de árboles.

Es importante notar que para que el código funcione como está en la plantilla,
las variables `base_trees`, `fruit_per_tree` y `reduction` serán variables
globales, por lo que tendrás que usar la palabra `global`, de forma conveniente,
en `main`.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público
o interno (no privado), creado bifurcando (forking) el repositorio de
plantilla de este ejercicio. La entrega de la práctica se hará subiendo
commits a ese repositorio git, como hemos hecho en prácticas anteriores.

Al terminar, comprueba en ETSIT GitLab que está el fichero con tu solución.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_aprox.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_aprox.py
```

En el repositorio plantilla hay además un fichero `.gitlab-ci.yml`, que hará que el sistema CI (Continuous Integration) de GitLab ejecute automáticamente los tests cada vez que se suba un nuevo commit. Puedes ver el resultado en la opción correspondiente del menú vertical de GitLab (cuando estés viendo tu repositorio).

### <a name="cantidades">Cantidades</a>

* **Fecha de entrega:** 23 de noviembre de 2022, 23:59
* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/cantidades

Vamos a realizar un programa que gestione los artículos (items) y
cantidades que tenemos en una despensa.
Las cantidades serán números enteros (que representarán cualquier cosa
que sea relevante), y los artículos serán cosas que se pueden guardar en una despensa:
leche, arroz, pan, fideos... No nos importa, para cada artículo, qué representa el número
(puede ser el número de litros para la leche, de barras para el pan, o de kilos para el
arroz). Pero vamos a querer saber qué cantidad tenemos de cada artículo, y qué cantidad
tenemos en total, además de la lista de artículos que tenemos.

Realiza, por lo tanto, un programa, que se llame `quantities.py` que permita esta gestión. Para ello,
utiliza la plantilla `plantilla.py` que encontrarás en el repositorio de plantilla.

El programa `quantities.py` se llamará de la siguiente forma:

```commandline
python3 quantities.py <op> [<op> ...]
```

Esto es, se le pasarán como argumentos una o más operaciones (`<op>`).
Las operaciones podrán ser:

* `add <item> <quantity>`: Añade a la despensa el artículo `<item>`, con cantidad `<quantity>`.
Si el artículo ya estuviese en la despensa, la cantidad indicada sustituirá a la que había.
Por ejemplo, `add leche 3` causará que se apunte que en la despensa hay 3 unidades de leche.
Esta operación no mostrará nada en pantalla.

* 'items': Muestra la lista de artículos en la despensa, separados por espacios.
Por ejemplo `items` mostrará en pantalla `Items: leche pan arroz` si previamente
hemos metido en la despensa (con `add`) leche, pan y arroz (en cualquier cantidad).

* `all`: Muestra todo lo que hay en la despensa, cada artículo con su cantidad.
Por ejemplo `all` mostrará en pantalla `All: leche (3) pan (4)` si previamente hemos
metido en la despensa (con `add`) 3 unidades de leche y 4 de pan.

* `sum`: Muestra la suma (el total) de lo que hay en la despensa. Por ejemplo,
`sum` mostrará en pantalla `Sum: 7` si previamente hemos
metido en la despensa (con `add`) 3 unidades de leche y 4 de pan.

Por lo tanto, podremos pasar al programa listas de argumentos como por ejemplo:

```commandline
python3 quantities.py add leche 3 items
```

que mostrará como resultado:

```commandline
Items: leche
```

O listas de argumentos más complejas, como:

```commandline
python3 quantities.py add leche 2 add pan 3 items sum add arroz 4 sum all 
```

que mostrará como resultado:

```commandline
Items: leche pan
Sum: 5
Sum: 9
All: leche (2) pan (3) arroz (4)
```

El programa podrá comprobar que cuando tenga que tener un argumento que sea
un entero, lo sea, y también que los comandos tengan el formato adecuado,
terminando con un mensaje en ese caso.

Ten en cuenta que, según se indica en la plantilla, el programa principal no
hará nada salvo llamar a la función `main`. Para realizar su trabajo, `main`
irá extrayendo de `sys.argv` el nombre de la siguiente operación que haya en los argumentos,
e irá llamando a las funciones de operación (`op_add`, `op_items`, `op_all`
y `op_sum`), que si lo necesitan leerán más argumentos de `sys.argv` (`op_add`
tendrá que hacer esto), y realizarán lo indicado para esa operación.

Los artículos se almacenarán en un diccionario llamado `quantities.items`
(esto es, un diccionario llamado `items`, definido en el entorno global de
tu programa `quantities.py`).

Para extraer el primer argumento de la lista `sys.argv` puede usarse la 
función `pop`, disponible en todas las listas, que extrae el primer elemento
de la lista si se le pasa el parámetro `0`. Por ejemplo, la instrucción

```python
op = sys.argv.pop(0)
```

extrae el primer elemento de `sys.argv` y lo pone en la variable `op`.
Si `sys.argv` tenía `['add', 'items', 'sum']`, esta lista pasará a tener
`['items', 'sum']` y la variable `op` pasará a tener `'items'`.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público
o interno (no privado), creado bifurcando (forking) el repositorio de
plantilla de este ejercicio. La entrega de la práctica se hará subiendo
commits a ese repositorio git, como hemos hecho en prácticas anteriores.

Al terminar, comprueba en ETSIT GitLab que está el fichero con tu solución.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_quantities.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_quantities.py
```

En el repositorio plantilla hay además un fichero `.gitlab-ci.yml`, que hará que el sistema CI (Continuous Integration) de GitLab ejecute automáticamente los tests cada vez que se suba un nuevo commit. Puedes ver el resultado en la opción correspondiente del menú vertical de GitLab (cuando estés viendo tu repositorio).

### <a name="cantidades">Búsqueda de palabras</a>

* **Fecha de entrega:** 30 de noviembre de 2022, 23:59
* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/buscapalabras

Realiza un programa, que se llame `searchwords.py`, al que se proporcionará como argumentos en la línea de comandos una palabra y una lista de palabras. El programa escribirá en pantalla al lista de palabras ordenada, y la posición de la palabra en la lista de palabras ordenada (empezando por 0). Por ejemplo:

```commandline
python3 searchwords.py Hola hola adios voy vengo
adios Hola vengo Voy
1
```

o, otro ejemplo:

```commandline
python3 searchwords.py Hola voy hola adios vengo
adios Hola vengo Voy
1
```


La lista de palabras se ordenará sin tener en cuenta mayúsculas o minúsculas (esto es, tanto la `a` como la `A` irán antes de la `b` o la `B`).

El programa debe usar el esquema que muestra el fichero `plantilla.py` que puedes encontrar en el repositorio de plantilla de esta práctica. En él hay varias funciones:

* El programa principal está en la función `main`, como ya hemos hecho en otras prácticas.

* La búsqueda la hace la función `search_word`, que recibe como parámetros una palabra (que es la palabra a buscar) y una lista ordenada de palabras (que es la lista donde se va a buscar la posición de la palabra). Esta función devuelve la posición donde se ha encontrado la palabra en la lista ordenada, o levanta la excepción `Exception` si no la ha encontrado.

Como la lista de palabras que se pasará como argumento al programa puede no estar ordenada, antes de que `main` pueda llamar a `search_word` tendrá que ordenar la lista. Para eso usaremos la función `sort`, del módulo `sortwords` (disponible en el repositorio plantilla). Igualmente, para mostrar la lista de palabras ordenadas, usaremos `show`, también de `sortwords`.

La función `main` tendrá que comprobar también:

* Que se han pasado al programa al menos dos argumentos (además del nombre del programa) en la línea de comandos (que sería una palabra a buscar, y una lista de una palabra donde hacer la búsqueda). Si no es así, debe llamar a `sys.exit` con un mensaje de error adecuado.

* Que no se ha levantado la excepción `Exception` en la función `search_word`. Si se hubiera levantado, tiene que capturarla, y terminar el programa llamando a `sys.exit`.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`, siguiendo el mismo proceso que en el ejercicio de entrega anterior (en su enunciado tienes detalles sobre el proceso).

Al terminar, comprueba en ETSIT GitLab que el fichero `searchwords.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en ETSIT GitLab como "público" o "interno", para que podamos recoger la práctica.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_searchwords.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_searchwords.py
```

En el repositorio plantilla hay además un fichero `.gitlab-ci.yml`, que hará que el sistema CI (Continuous Integration) de GitLab ejecute automáticamente los tests cada vez que se suba un nuevo commit. Puees ver el resultado en la opción correspondiente del menú vertical de GitLab (cuando estés viendo tu repositorio).

### <a name="final">Proyecto final</a>

* **Fecha de entrega:** 16 de enero de 2023, 23:59
* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/final

El proyecto final consiste en la creación de varios módulos de tratamiento digital de sonido, y algunos programas que usan esos módulos para establecer cadenas de procesado digital.

El proyecto final está estructurada en:

* Requisitos mínimos. Cumplirlos es necesario para que el proyecto final sea considerado como aprobado, en cuyo caso seá puntuado con 2.0.
* Requisitos opcionales. Pueden ser puntuados con hasta 3.0 (adicionales a la puntuación de los requisitos mínimos).

#### Código de partida

El repositorio de plantilla de la práctica tiene:

* Código de ejemplo. Directorio `ejemplo` con los programas `play_freq.py`, `play_wav.py`, `play_bars.py` y `sound_record.py`.

* Código de comienzo para la realización de requisitos mínimos. Este código tendrá que ser completado y ampliado para realizar los requisitos mínimos del proyecto. Es importante mantener la estructura de este código (en cuanto a la división en módulos y a su estructura interna, como por ejemplo los métodos que hay en ellos):
  * `sources.py`: métodos (funciones) para producir sonido digital de distintas formas (fuentes de sonido). Incluye un método `sin` que produce una señal sinusoidal de una frecuencia dada, y un método `constant` que produce una señal constante. Ambas funciones devuelven el sonido como un vector (array) de muestras, donde cada muestra es un número entero de dos bytes. Ambas funciones aceptan como primer parámetro la duración del sonido a producir. También hay algunos métodos auxiliares, cuyo nombre comienza por `_`.
  * `sinks.py`: métodos (funciones) para almacenar o reproducir de alguna forma el sonido (sumideros de sonido). Incluye un método `play` que reproduce el sonido, y un método `draw` que escribe en pantalla líneas de asteriscos correspondientes al valor medio de conjuntos de muestras de la señal. Ambos métodos aceptarán como primer parámetro el sonido a reproducir o a mostrar. También hay algunos métodos auxiliares, cuyo nombre comienza por `_`.
  * `source_sink`: programa que acepta en línea de comandos tres argumentos: el primero será una fuente (source), el segundo un sumidero (sink), y el tercero una duración (en segundos). El programa producirá sonido con la fuente indicada, de la duración indicada, y lo reproducirá o mostrará usando el sumidero indicado.
  * `config.py`: constantes de configuración, y otras constantes utilizadas por los módulos.

#### Requisitos mínimos


* Método `square`. Completar el módulo `sources.py` con un método (función), `square`, que produzca una señal cuadrada: durante un tiempo estará a la máxima amplitud, a continuación, durante el mismo tiempo, a la mínima amplitud (negativa), luego de nuevo a la máxima, y así sucesivamente. La función aceptará dos parámetros: `duration`, que será la duración, en segundos, del sonido producido, y `freq`, que será la frecuencia de la señal (cuántas veces por segundo la señal estará en el máximo). La signatura de esta función será:

```python
def square(duration: float, freq: float):
```

* Método `store`. Completar el módulo `sinks.py` con un método (función), `store` que almacene el sonido en un fichero en formato WAV. La función aceptará dos parámetros: `sound`, que será un array de muestras de sonido, y `path`, que será el camino (nombre completo) del fichero en que se almacenará el sonido. La signatura de esta función será:

```python
def store(sound, path: str):
```

* Método `ampli`. Crear un nuevo módulo, `processes.py` que incluirá un método (función), `ampli` que amplificará el sonido multiplicando el valor de cada muestra por un número dado. Si el resultado de la multiplicación es mayor que la amplitud máxima, o menor que la amplitud mínima, el valor de la muestra amplificada será ese máximo o mínimo, respectivamente. La función aceptará dos parámetros: `sound` (el sonido a amplificar) y `factor` (el factor de amplificación, que será un número `float`, y que se usará para multimplicar cada muestra por él). La signatura de esta función será:

```python
def ampli(sound, factor: float):
```

* Método `reduce`. Crear en el módulo `processes.py` un método (función), `reduce` que reducirá el número de muestras de sonido quitando una de cada tantas que se le indica. Por ejemplo, si se indica "3", quitará una de cada tres: la tercera, la sexta, la novena, y así sucesivamente. En este caso, por "quitar" se entiende que la muestra no estará en el sonido que devuelva la función. Naturalmente, la duración del sonido quedará reducida en la misma proporción (si un sonido de tres segundos se reduce "uno de cada tres", quedará en dos segundos). La función aceptará dos parámetros: `sound` (el sonido a reducir) y `factor` (el factor de reducción, que será un número `int` mayor que 0). La signatura de esta función será:

```python
def reduce(sound, factor: int):
```

* Método `extend`. Crear en el módulo `processes.py` un método (función), `extend` que extenderá el número de muestras de sonido añadiendo una más cada tantas que se le indica. Por ejemplo, si se indica "3", añadirá una cada tres: tras las tres primeras una cuarta, tras las tres siguientes una octava, y así sucesivamente. En este caso, por "añadir" se entiende que la nueva muestra se colocará después de las indicadas, y se calculará como la media entre la última de esas muestras, y la muestra siguiente. Naturalmente, la duración del sonido quedará extendida en la misma proporción (si un sonido de tres segundos se extiende "uno de cada tres", quedará en cuatro segundos). La función aceptará dos parámetros: `sound` (el sonido a reducir) y `factor` (el factor de extensión, que será un número `int` mayor que 0). La signatura de esta función será:

```python
def extend(sound, factor: int):
```

* Extensión de `source_sink.py`. Se extenderá este módulo para que acepte como posible fuente (source) de sonido `square` (además de las que ya acepta) y como posible sumidero (sink) de sonido `store`. Si se especifica `square`, se ejecutará el mérodo `square` con una frecuencia de 400 hertz, y si se especifica `store`, se ejecutará el método `store` con el nombre de fichero `stored.wav`. Por lo tanto, el programa se ejecutará con las mismas opciones que ya tiene, pero aceptando estos nuevos valores. Por lo demś, se comportará igual.

* Creación de `source_process_sink.py`. Se creará un nuevo módulo que pueda funcionar como progrma principal, con la misma estructura que `source_sink.py` (una función `main` que tendrá el programa principal, y que se ejecutará cuando el módulo sea invocado como programa principal). Este programa ejecutará primero una fuente, después un procesador, y por último un sumidero de sonido. Admitirá como argumentos un nombre de fuente, uno de procesador, uno de sumidero, y una duración. Los nombres de fuente, procesador y sumidero serán los nombres de las funciones correspondientes en `sources.py`, `processes.py` y `sinks.py`. El programa, por tanto, se ejecutará como sigue:

```python
python3 source_process_sink.py <source> <process> <sink> <duration>
```

  En caso de que no se indique el número correcto de argumentos, el programa terminará y mostrará un mensaje indicando el fomato de uso. Puee usarse cualquier valor razonable para los parámetros no especificados de las funciones de fuente, procesado y sumidero (frecuencias, nombres de fichero, etc.).

#### Notas sobre los requisitos mínimos

* Para la inplementación del método `square` puede ser conveniente pensar que cada muestra generada va a valer `max_amp` o `-max_amp`, dependiendo de que esté en la parte alta o baja de cada "semiciclo". Cada ciclo comenzará con una "parte alta" (semiciclo alto) donde las muestras valdrán `max_amp`, seguido de una "parte baja" (semiciclo bajo) donde las muestras valdrán `-max_amp`. Y a continuación se repite en el siguiente ciclo, y así sucesivamente. Por lo tanto, bastará con saber a qué semiciclo pertenece una muestra para saber su valor.

Para calcular en qué semiciclo está una muestra, podemos empezar por calcular la duración de cada semiciclo. Un ciclo completo durará durará `1/freq` (en segundos). Luego un semiciclo (la mitad de un ciclo) será `(1/freq)/2`, esto es, `1/(freq*2)` segundos. Además, podemos ver que los cemiciclos pares (si empezamos a numerar por 0) serán altos, y los semiciclos impares serán bajos.  Por lo tanto, si una muestra está en un semiciclo par, su valor será `max_amp`, y si está en un semiciclo impar, su valor será `-max_amp`. Así pues, sólo queda saber cuándo si una muestra dada está en semiciclo par o impar.

Igual que hacíamos por ejemplo en la función `sin`, podemos calcular el tiempo `t` que corresponde a cada muestra, conociendo su posición `nsample` en el array que tiene todas las muestras:

```
t = nsample / config.samples_second
```

Una vez que lo conocemos, podemos dividir el tiempo de la muestra por la duración de un semiperiodo. Si la parte entera del número resultante es par, el semiciclo es par. Si es impar, el semiciclo es impar. Esta parte entera la podemos calcular simplemente convirtiendo a entero el número resultante. Para saber si es par o impar, podemos utilizar el resto de la división entera. El código resultante podría ser similar al siguiente, para una muestra `nsample` (atención, este código no está probado, y es muy posible que no funcione tal cual):

```
subcycle_dur = 1 / (freq * 2)
t = nsample / config.samples_second
subcycle_n = int(t / subcycle_dur)
if (subcycle_n % 2) == 0:
    sound[nsample] = config.max_amp
else:
    sound[nsample] = - config.max_amp
```

#### Requisitos opcionales

Para cumplir los requisitios opcionales, se podrá elegir entre los siguientes:

* Método `readfile` (hasta 0.5). Completar el módulo `sources.py` con un método (función), `readfile`, que lea sonido de un fichero en formato WAV. Para hacer pruebas, pude usarse el fichero `recording.wav` que se incluye en el repositorio plantilla. En general el fichero WAV tendrá dos canales (izquierdo y derecho), pero el sonido digital producido tendrá solo el contenido del primer canal que se encuentre. Esto es, cada muestra constará de un solo entero, no de una tupla con dos enteros, como normalmente se leerá del fichero WAV. Pueden verse detalles sobre cómo se lee el fichero, y cómo se acceden a las muestras de los dos canales en el ejemplo `examples/play_wav.py` y `examples/play_bars.py`. La función aceptará dos parámetros: `duration`, que será la duración, en segundos, del sonido leido del principio del fichero, y `path`, que será el camino (nombre completo) del fichero a leer. La signatura de esta función será:

```python
def readfile(duration: float, path: str):
```

  Para poder completar este requisito, es conveniente ver la [documentación de `soundfile.read`](https://pysoundfile.readthedocs.io/en/latest/#soundfile.read).

* Método 'round' (hasta 0.5). Completar el módulo `processes.py` con un método (función), `round`, que redondeará el sonido calculando para cada muestra, la media con las muestras que le rodean. Por ejemplo, si se especifica que se redondee con dos, se usarán para calcular la media: las dos que le preceden, las dos que le siguen, y la propia muestra. Así, para calcular el nuevo valor de la muestra 18, calcularíamos la media de los valores de las muestras 16, 17, 18, 19 y 20. La función aceptará dos parámetros: `sound` (el sonido a reducir) y `samples` (el número de muestras de redondeo). La signatura de esta función será:

```python
def round(sound, samples: int):
```

* Creación de 'sound_process.py' (hasta 1.0). Se creará un nuevo módulo que pueda funcionar como progrma principal, con la misma estructura que `source_sink.py` (una función `main` que tendrá el programa principal, y que se ejecutará cuando el módulo sea invocado como programa principal). Este programa ejecutará primero una fuente, después un procesador, y por último un sumidero de sonido. Admitirá como argumentos un nombre de fuente, sus parámetros, un nombre de procesador, sus parámetros, un nombre de sumidero, sus parámetros, y una duración. Los nombres de fuente, procesador y sumidero serán los nombres de las funciones correspondientes en `sources.py`, `processes.py` y `sinks.py`. El programa, por tanto, se ejecutará como sigue:

```python
python3 sound_process.py <source> <source_args> <process> <process_args> <sink> <sink_args> <duration>
```

  Por ejemplo, una ejecución podría ser:

```python
python3 sound_process.py sin 400 amp 0.5 draw 0.001 3
```

  En este caso, se ejecutaría con la función fuente `sin`, a la que se pasarían los parámetros 3 (último argumento, duración del sonido), y 400 (argumento de `sin`, que corresponde con el parámetro `freq`). El sonido resultante se pasaría a la función procesadora `amp` (ese sonido producido por `sin` sería su parámetro `sound`), y 0.5 (argumento de `amp`) como parámetro `factor`. El sonido resultante se pasaría a la función `draw` (ese sonido producido por `amp` sería su parámetro `sound`), y 0.001 (argumento de `draw`) como parámetro `period`.

  En caso de que no se indique el número correcto de argumentos, el programa terminará y mostrará un mensaje indicando el fomato de uso. Puee usarse cualquier valor razonable para los parámetros no especificados de las funciones de fuente, procesado y sumidero (frecuencias, nombres de fichero, etc.).

* Creación de 'sound_processes.py' (hasta 1.0). Se creará un nuevo módulo que pueda funcionar como progrma principal, con la misma estructura que `source_sink.py` (una función `main` que tendrá el programa principal, y que se ejecutará cuando el módulo sea invocado como programa principal). Este programa ejecutará primero una fuente, después un número arbitrario de procesadores, incluyendo la posibildad de que no se ejecute ningún procesador, y por último un sumidero de sonido. Admitirá como argumentos un nombre de fuente, sus parámetros, uno, ninguno, o varios nombres de procesador con sus parámetros, y finalmente un nombre de sumidero, sus parámetros, y una duración. Los nombres de fuente, procesadores y sumidero serán los nombres de las funciones correspondientes en `sources.py`, `processes.py` y `sinks.py`. El programa, por tanto, se ejecutará como sigue:

```python
python3 sound_process.py <source> <source_args> [<process> <process_args> ]* <sink> <sink_args> <duration>
```

Donde `[<process> <process_args> ]*` indica que puede haber cero, uno o variso grupos `<process> <process_args>`

  Por ejemplo, las siguientes serían ejemplos de ejecución:

```python
python3 sound_processes.py sin 400 draw 0.001 3
python3 sound_processes.py sin 400 amp 0.5 draw 0.001 3
python3 sound_processes.py sin 400 amp 0.5 reduce 2 draw 0.001 3
```

* Creación de `add` (hasta 1.0). Completar el módulo `processes.py` con un método (función), `add`, que realizará la suma entre lo que reciba de la etapa anterior, y una fuente de señal que se le ponga como argumento (incluyendo sus parámetros). Por ejemplo:

```python
python3 sound_processes.py sin 400 add sin 200 draw 0.001 3
```

Esta ejecución produciría una señal sinusoidal de 400 ciclos por segundo, a continuación la sumará con el resultado de producir una señal sinusoidal de 200 ciclos por segundo, y la señal resultante la mostará utilizando el módulo `draw` con un parémtro `0.001`.

Además, los alumnos pueden proponer otras mejoras y funcionalidades (requisitos opcionales propios). Es importante comentar estas mejoras y funcioanalidades con los profesores (normalmente por correo electrónico) para asegurarse de que los profesores consideran qu eson adecuadas. Si no, hay riesgo de que estas mejoras y funcionalidades no sean valoradas como el alumno espera, o incluso que no sean consideradas. Por ejemplo, se pueden proponer nuevas funciones de proceso de señal, nuevas fuentes de señal, o nuevos sumiderso de señal, que normalmente se puntuarán entre 0.5 y 1.0 cada uno. Pero para saber si una propuesta concreta se considerará, y hasta cuánto se puntuará, es necesario preguntar a los profesores.

La puntuación máxima para el conjunto de las opciones es de 3 puntos. Es importante tener en cuenta que una cualquiera de las opciones poría puntuarse por su puntuación máxima, o por una menor, si se estimara que presenta algún problema (por ejemplo, no funcionase adecuadamente en todas las situaciones). 

#### Entrega

El proyecto final ha de ser entregado en un repositorio de acceso público
o interno (no privado), creado bifurcando (forking) el repositorio de
plantilla. La entrega de la práctica se hará subiendo
commits a ese repositorio git, como hemos hecho en prácticas anteriores.

La entrega deberá incluir un fichero `entrega.md`, que tendrá al menos los siguientes datos, en este orden:

* Nombre completo del alumno que realiza la entrega
* Apartado "Requisitos mínimos": listado de los requisitos mínimos que se han implementado, indicando el nombre que aparece en el enunciado (por ejemplo "Método square").
* Apartado "Requisitos opcionales": listado de los requisitos opcionales que se han implementado, indicando el nombre que aparece en el enunciado (por ejemplo "Método readfile").
* Apartado "Requisitos opcionales propios": listado de los requisitos opcionales que se han implementado que no están en el listado del enunciado. En este caso, para cada uno de ellos, incluir una breve descripción, que incluya cómo se pueden ejecutar.

Al terminar, comprueba en tu repositorio en ETSIT GitLab que están correctamente todos los ficheros que quieres entregar en el repositorio de entrega.


### <a name="final2">Proyecto final (convocatoria de junio)</a>

[Enunciado aún en proceso de preparación, puede sufrir pequeños cambios. Por favor, indícamos si encuentras algún error.]

* **Fecha de entrega:** 15 de junio de 2023, 23:59
* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/final2

El proyecto final consiste en la creación de varios módulos de tratamiento digital de sonido, y algunos programas que usan esos módulos para establecer cadenas de procesado digital.

El proyecto final está estructurada en:

* Requisitos mínimos. Cumplirlos es necesario para que el proyecto final sea considerado como aprobado, en cuyo caso seá puntuado con 2.0.
* Requisitos opcionales. Pueden ser puntuados con hasta 3.0 (adicionales a la puntuación de los requisitos mínimos).

#### Código de partida

El repositorio de plantilla de la práctica tiene:

* Código de ejemplo. Directorio `ejemplo` con los programas `play_freq.py`, `play_wav.py`, `play_bars.py` y `sound_record.py`.

* Código de comienzo para la realización de requisitos mínimos. Este código tendrá que ser completado y ampliado para realizar los requisitos mínimos del proyecto. Es importante mantener la estructura de este código (en cuanto a la división en módulos y a su estructura interna, como por ejemplo los métodos que hay en ellos):
  * `sources.py`: métodos (funciones) para producir sonido digital (señales) de distintas formas (fuentes de sonido). Incluye un método `sin` que produce una señal sinusoidal de una frecuencia y un número de muestras dados, y otro `load` que carga el sonido de un fichero audio. En ambos casos, el método (función) devuelve el sonido como una lista de muestras, donde cada muestra es un número entero. Esta será en general la forma en que se almacenará el sonido en todo el proyecto.
  * `sinks.py`: métodos (funciones) para mostrar de alguna forma el sonido (sumideros de sonido). Incluye un método `play` que reproduce el sonido, y un método `draw` que escribe en pantalla líneas de asteriscos correspondientes al valor de cada muestra. En ambos casos, los métodos admiten sonido como listas de números enteros, como se han indicado antes.
  * `processors.py`: métodos (funciones) para procesar el sonido (procesadores de sonido). Incluye un método `ampli` que amplifica el sonido. Los procesadores admitirán y producirán sonido como listas de números enteros, como se han indicado antes.
  * `config.py`: constantes de configuración, y otras constantes utilizadas por los módulos.
  * Ficheros que comienzan por `example_`: ejemplos de uso de las funciones que actúan como fuente, procesador y sumidero.
  
#### Requisitos mínimos

Para cumplir los requisitos mínimos, el proyecto entregado ha de cumplir los siguientes apartados. Deben usarse exactamente los nombres de funciones, parámetros, ficheros y argumentos que se indican.

* **constant**. Método `constant`. Completar el módulo `sources.py` con un método (función), `constant`, que produzca una señal (sonido) constante (todas las muestras del mismo valor). La función aceptará dos parámetros: `nsamples`, que será el número de muestras a generar, y `level`, que será el nivel, el valor de cada muestra, un número entero. Si el valor absoluto del nivel es mayor que `config.max_amp`, se generarán muestras con el valor `config.max_amp` (si el nivel es positivo) o `-config.max_amp` (si el nivel es negativo). El resultado de la función será una lista de `nsamples` enteros. La signatura de esta función será:

```python
def constant(nsamples: int, level: int):
```

* **square**. Método `square`. Completar el módulo `sources.py` con un método (función), `square`, que produzca una señal cuadrada: durante un número de muestras estará a la máxima amplitud, a continuación, durante el mismo número de muestras, a la mínima amplitud (negativa), luego de nuevo a la máxima, y así sucesivamente. La función aceptará dos parámetros: `nsamples`, que será el número de muestras a generar, y `nperiod`, que será el número de muestras de un periodo (desde que empieza la señal cuadrada hasta justo antes de que se repita). Esto es, la señal estará al máximo valor durante `nperiod/2` muestras, luego al valor mínimo durante `nperiod/2` muestras, luego de nuevo al máximo, y así sucesivamente. El resultado de la función será una lista de `nsamples` enteros. La signatura de esta función será:

```python
def square(nsamples: int, nperiod: int):
```

  Por ejemplo, si se llama a la función como `square(10, 4)`, y `config.max_amp` vale 20000, el resultado será:

```  
[ 20000, 20000, -20000, -20000, 20000, 20000, -20000, -20000, 20000, 20000 ]
```

* **show**. Método `show`. Completar el módulo `sinks.py` con un método (función), `show` que muestra en pantalla (usnado `print`) las muestras de una señal (sonido), como números enteros. La función aceptará dos parámetros: `sound`, que será una lista de muestras de sonido (enteros), y `newline`, que será un booleano indicando si se debe poner un fin de línea después de cada muestra. Si `newline` es `False`, se mostrarán las muestras como enteros separados por comas. Si es `True`, cada entero aparecerá en una línea. La signatura de esta función será:

```python
def show(sound, newline: bool):
```
Lo mostrado en pantalla, para `newline=False` será de estilo:

```
3445,14333,-2344,...
```
Si `newline=True` lo mostrado en pantalla será del estilo:

```
3445
14333
-2344
...
```

* **info**. Método `info`. Completar el módulo `sinks.py` con un método (función), `info` que muestra en pantalla (usnado `print`) información sobre la señal (sonido). La función aceptará un parámetro: `sound`, que será una lista de muestras de sonido (enteros). La función mostrará, en el formato que se indica más abajo: el número de muestras del sonido, el valor máximo de las muestras (y el número de la última muestra que tenga ese valor), el valor mínimo de las muestras (y el número de la última muestra que tenga ese valor), el valor medio de todas las muestras, el número de muestras positivas, el número de muestras negativas, y el número de muesrtas con valor certo  La signatura de esta función será:

```python
def info(sound):
```
Lo mostrado en pantalla, será de este estilo:

```
Samples: 41100
Max value: 200 (sample 23)
Min value: -12500 (sample 5632)
Mean value: -456
Postive samples: 1100
Negative samples: 39000
Null samples: 1000
```

Por ejemplo, si la señal es `[0, 200, 0, -200, 0, 200, 0, -200]`, se mostraría:

```
Samples: 8
Max value: 200 (sample 5)
Min value: -200 (sample 7)
Mean value: 0
Postive samples: 2
Negative samples: 2
Null samples: 4
```


* **shift**. Método `shift`. Crear un nuevo módulo, `processors.py` que incluirá un método (función), `shift` que "desplazará" el sonido, sumando al valor de cada muestra un valor de desplazamiento. Si el resultado del desplazamiwnto de una muestra es mayor que la amplitud máxima, o menor que la amplitud mínima, el valor de la muestra desplazada será ese máximo o mínimo, respectivamente. La función aceptará dos parámetros: `sound` (el sonido a desplazar, que será una lista de muestras de sonido) y `value` (el valor de desplazamiento, que será un número `int`, y que se usará para sumar cada muestra por él). La signatura de esta función será:

```python
def shift(sound, value: int):
```

* **trim**. Método `trim`. Crear en el módulo `procesors.py` un método (función), `trim` que eliminará un cierto número de muestrs de la señal (sonido), bien de su principio, bien de su final. Por ejemplo, si se indica "10", producirá una señal (sonido) igual, pero sin las 10 primeras o las 10 últimas muestras. Naturalmente, el número de muestras resultante quedará reducido en el número de muestras eliminadas. La función aceptará tres parámetros: `sound` (el sonido a reducir, que será una lista de muestras de sonido), `reduction` (el número de muetras a quitar, que será que será un número `int` mayor que 0), y `start` (un booleano que si es `True`, indica que se han de quitar las muestras del principio, y si es `False`, del final). Si `reduction` es mayor que el número de muestras en `sound`, se devolverá una lista vacía (con ninguna muestra). La signatura de esta función será:

```python
def trim(sound, reduction: int, start: bool):
```

* **repeat**. Método `repeat`. Crear en el módulo `processors.py` un método (función), `repeat` que producirá una señal (sonido) igual que la original, repetida el número de veces que se indique. Por ejemplo, si se indica una repetición de "3", se producirá un sonido que será igual al original, y luego tres copias de él (esto es, el resultante será la secuencia de cuatro veces el sonido original). Naturalmente, la duración del sonido quedará extendida con tantas muestras como tenga, multimplicada por el factor de repetición. La función aceptará dos parámetros: `sound` (el sonido a reducir) y `factor` (el factor de repetición, que será un número `int` mayor que 0). La signatura de esta función será:

```python
def repeat(sound, factor: int):
```

* **sound.py**. Creación de un programa `sound.py` que acepte como argumentos una fuente (source), un procesador (proocessor) y un sumidero (sink), cada uno con los parámetros que precise. El programa producirá primero un sonido (lista de muestras) usando la fuente especificada, luego aplicará el procesador especificado a ese sonido, y el resultado será a su vez pasado al sumidero, que también será ejecutado. Los nombres de los argumentos  serán los mismos que los de la función fuente, procesadora o sumidero (por ejemplo, para la función `repetr()` el nombre del argumento será `repeat`). Los parámetros que precise cada función (menos el parámetro `sound`, que no se especificará) irán directamente a continuación del nombre del módulo. Por ejemplo, en el caso de la función `repeat()`, para especificar `20` como parámetro `factor` se escribirán los argumentos `repeat 20`. Para la función `trim()`, que acepta dos parámetros, si queremos especificar 10 para el parémtro `reduction` y `false` para el parámetro `start` escribiremos `reduction 10 false`.

  Por ejemplo, para ejecutar `sound` con `sin` como fuente, `repeat` como procesador el parámetro `3`), e `info` como sumidero, escribiremos:

```commandline
python3 sound.py sin 100 600.0 repeat 3 info
```

  Para ejecutar con `load` como sumidero (con el parámetro `recording.wav`), `trim` como procesador (con los parámetros `15` y `True`), y `show` como sumidero (con el parámetro `newline` igual a `False`) escribiríamos:

```commandline
python3 sound.py load recording.wav trim 15 true show false
```

* **menu.py**. Creación de un programa `menu.py` que cuando sea ejecutado:
  * Primero muestre un menú con las opciones de fuente (source), permitiendo escribir una de ellas, que será la seleccionada (y si se escribe algo que no corresponda con una fuente, vuelva a pedir que la escribamos, hasta que sea una correcta).
  * Después, pregunte uno a uno el valor de los parámetros de esa fuente, permitiendo que los vayamos introduciendo (en esta parte no será preciso hacer control de errores: se supone que proporcionamos valores correctos). 
  * Después, muestre un menú con las opciones de sumidero (sink), permitiendo escribir una de ellas, que será la seleccionada (y si se escribe algo que no corresponda con un dumidero, vuelva a pedir que la escribamos, hasta que sea correcta).
  * Después, pregunte uno a uno el valor de los parámetros de ese sumidero (salvo `sound`), permitiendo que los vayamos introduciendo (en esta parte no será preciso hacer control de errores: se supone proporcionamos valores correctos).
  * Para terminar, ejecutará la fuente indicada con los paráemtros indicados, y con el sonido resultante ejecutará el sumidero indicado con los parámetros indicados.

  El menú mostrará como opciones los nombres de las funciones disponibles (para fuente o para sumidero, según el caso), y como parámetros los nombres de los parámetros.  Por ejemplo, lo siguiente podría ser una ejecución completa:

```commandline
python3 menu.py
Dime una fuente (load, sin, constant, square):
xxx
Dime una fuente (load, sin, constant, square):
yyy
Dime una fuente (load, sin, constant, square):
sin
Dame los parámetros para sin.
nsamples: 100
freq: 600.0
Dime un sumidero (play, draw, show, info):
zzz
Dime un sumidero (play, draw, show, info):
info
Ejecutando sin (100, 600.0) y info():
Samples: 100
Max value: ...
...
```

#### Requisitos opcionales

Para cumplir los requisitios opcionales, se podrá elegir entre los siguientes:

* **clean** (hasta 0.5). Método `clean`. Crear en el módulo `processors.py` un método (función), `clean` que producirá una señal (sonido) igual que la original, reduciendo a 0 las muestras que tengan un valor pequeño (menor, en valor absoluto, que uno dado). Por ejemplo, si se indica el valor "20", y el sonido es `[234, 456, 15, -12, -234]`, el resultado sería `[234, 456, 0, 0, -234]`. La función aceptará dos parámetros: `sound` (el sonido a "limpiar") y `level` (el nivel que indica que, si el valor absoluto de la muestra es menor, la muestra ha de ser puesta a cero). La signatura de esta función será:

```python
def clean(sound, level: int):
```

* **round** (hasta 0.5). Método 'round'. Completar el módulo `processors.py` con un método (función), `round`, que redondeará el sonido calculando para cada muestra, la media con la muestra precedente y la muestra siguiente (si es la primera o la última muestra, se dejará tal cual). Por ejemplo, para calcular el nuevo valor de la muestra 18, calcularíamos la media de los valores de las muestras 17, 18 y 19. La función aceptará un parámetro: `sound` (el sonido a redondear, que será una lista de muestras, siendo cada muestra un número entero). La signatura de esta función será:

```python
def round(sound):
```

* **sound2.py** (hasta 1.0). Programa `sound2.py`, que funcionará como `sound.py`, pero permitiendo que se pueda poner ninguno, uno, o varios procesadores. Por ejemplo, ss podrá ejecutar con sólo una fuente y un sumidero, como por ejemplo:

```commandline
python3 sound.py sin 100 600.0 info
```

o igual que  `sound.py`, con una fuente, un procesador y un sumiderto, como:

```commandline
python3 sound.py sin 100 600.0 repeat 3 info
```
o con una fuente, dos procesadores y un sumidero, como por ejemplo:

```commandline
python3 sound.py sin 100 600.0 repeat 3 trim 15 show false
```

o con una fuente, tres procesadores y un sumidero, como por ejemplo:

```commandline
python3 sound.py sin 100 600.0 repeat 3 trim 15 ampli 2.0 show false
```

o con una fuente, cuatro procesadores y un sumidero, como por ejemplo (en este caso, repitiendo procesador, pero podría ser con otro distinto):

```commandline
python3 sound.py sin 100 600.0 repeat 3 ampli 0.5 trim 15 ampli 2.0 show true
```

o con una fuente, más procesadores y un sumidero.

* **menu2.py** (hasta 1.0). Programa `menu2.py`, que funcione como `menu.py` pero detectando cuándo uno cualquiera de los parámetros indicados para una fuente o us sumidero no es correctos (por ejemplo, debería ser un número entero, pero se ha escrito `xxx`, o debería ser `true` o `false`, pero se ha escrito `3`), y en ese caso pidiendo que lo volvamos a escribir, hasta que sea un parámetro correcto.

* **menu3.py** (hasta 0.8). Programa `menu3.py`, que funcione como `menu.py` pero permitiendo elegir también un procesador, con sus parámetros, además de fuente y sumidero.

* **add** (hasta 0.8). Creación de la función `add`. Completar el módulo `processors.py` con un método (función), `add`, que realice la suma de dos señales (sonidos) que reciba como parámetros. Para ello, realizará la suma entre las muestras de las dos señales que tengan igual índice. Si el valor absoluto de la suma es mayor que la amplitud máxima, quedará en la amplitud máxima (si es positiva) o mínima (si es negativa). Si una de las señales tiene más muestras que la otra, las muestras de la señal más larga sin equivalente en la más corta quedarán tal cual. Por ejemplo, si aplicamos `add` a [100, -100, 20] y [200, -200] el resultado será [300, -300, 20]. La signatura de esta función será:

```python
def add(sound1, sound2):
```

* **soundadd.py** (hasta 1.0). Programa `soundadd.py` (sólo en caso de haber construido la función `add`). Crear `soundadd.py` que funciona como `sound.py` pero con dos fuentes (sources), que serán los primeros argumentos (junto con sus parámetros) que se pasarán al programa. El programa ejecutará primero las dos fuentes, a continuación las sumará usando al función `add`, luego aplicará al resultado el procesador que se haya especificado, y por último, el sumidero. Así, en el siguiente ejemplo, por un lado se cargaría una señal del fichero `recording.wav`, por otro se produciría una señal sinusoidal de 100 muestras y 600 Hz, luego se sumarían ambas señales usando `add`, se repetiría el resultado 3 veces, y por último se mostraría información sobre el resultado final, usando `info`:

```commandline
python3 sound.py load recording.wav sin 100 600.0 repeat 3 info
```

Además, los alumnos pueden proponer otras mejoras y funcionalidades (requisitos opcionales propios). Es importante comentar estas mejoras y funcioanalidades con los profesores (normalmente por correo electrónico) para asegurarse de que los profesores consideran qu eson adecuadas. Si no, hay riesgo de que estas mejoras y funcionalidades no sean valoradas como el alumno espera, o incluso que no sean consideradas. Por ejemplo, se pueden proponer nuevas funciones de proceso de señal, nuevas fuentes de señal, o nuevos sumiderso de señal, que normalmente se puntuarán entre 0.5 y 1.0 cada uno. Pero para saber si una propuesta concreta se considerará, y hasta cuánto se puntuará, es necesario preguntar a los profesores.

La puntuación máxima para el conjunto de las opciones es de 3 puntos. Es importante tener en cuenta que una cualquiera de las opciones poría puntuarse por su puntuación máxima, o por una menor, si se estimara que presenta algún problema (por ejemplo, no funcionase adecuadamente en todas las situaciones). 

#### Entrega

El proyecto final ha de ser entregado en un repositorio de acceso público
o interno (no privado), creado bifurcando (forking) el repositorio de
plantilla. La entrega de la práctica se hará subiendo
commits a ese repositorio git, como hemos hecho en prácticas anteriores.

La entrega deberá incluir un fichero `entrega.md`, que tendrá al menos los siguientes datos, en este orden:

* Nombre completo del alumno que realiza la entrega
* Apartado "Requisitos mínimos": listado de los requisitos mínimos que se han implementado, indicando el nombre que aparece en el enunciado (por ejemplo "Método square").
* Apartado "Requisitos opcionales": listado de los requisitos opcionales que se han implementado, indicando el nombre que aparece en el enunciado (por ejemplo "Método readfile").
* Apartado "Requisitos opcionales propios": listado de los requisitos opcionales que se han implementado que no están en el listado del enunciado. En este caso, para cada uno de ellos, incluir una breve descripción, que incluya cómo se pueden ejecutar.

Al terminar, comprueba en tu repositorio en ETSIT GitLab que están correctamente todos los ficheros que quieres entregar en el repositorio de entrega.