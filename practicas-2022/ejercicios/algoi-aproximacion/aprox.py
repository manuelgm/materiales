#!/usr/bin/env pythoh3

'''
Cálculo del número óptimo de árboles.

Producción de un campo con árboles frutales. Si hay base_tree
árboles, producen cada uno fruit_per_tree kilos de fruta cada uno.
Por cada árbol más que se planta, la producción de cada árbol
se reduce en reduction kilos de fruta.
Calcular el número óptimo de árboles entre dos números dados.
'''

import sys

def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees):

    productions = []
    for trees in range(min_trees, max_trees+1):
        production = compute_trees(trees)
        productions.append((trees, production))
    return productions

def read_arguments():

    if len(sys.argv) != 6:
        sys.exit(f"Usage: {sys.argv[0]} <base_trees> <fruit_per_tree> <reduction> <min> <max>")
    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])
    except ValueError:
        sys.exit("All arguments must be integers")

    return base_trees, fruit_per_tree, reduction, min, max


def main():
    global base_trees
    global fruit_per_tree
    global reduction

    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)

    best_production = 0
    best_trees = 0

    for case in productions:
        (trees, prod) = case
        print(trees, prod)
        if prod > best_production:
            best_trees = trees
            best_production = prod

    print(f"Best production: {best_production}, for {best_trees} trees")

if __name__ == '__main__':
    main()
