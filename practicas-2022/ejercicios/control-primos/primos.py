#!/usr/bin/env pythoh3

'''
Program to compute odd numbers between two given.
'''

limit: int = int(input("Dame un número entero no negativo: "))

print("Números primos iguales o menores:", end='')

for number in range(1, limit+1):
    # Every number is prime until we can prove otherwise
    is_prime: bool = True
    # Let's check if we find an exact divisor
    for divisor in range(2, number):
        if (number % divisor) == 0:
            # We found a divisor, number is not prime
            is_prime = False
            break;
    if is_prime:
        print('', number, end='')

print()
