#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest

import segundos2

class TestDays(unittest.TestCase):

    def test_normal(self):
        dias, segs = segundos2.convert_days(260000)
        self.assertEqual(3, dias)
        self.assertEqual(800, segs)

    def test_zero(self):
        dias, segs = segundos2.convert_days(0)
        self.assertEqual(0, dias)
        self.assertEqual(0, segs)

    def test_exact(self):
        dias, segs = segundos2.convert_days(259200)
        self.assertEqual(3, dias)
        self.assertEqual(0, segs)

    def test_one(self):
        dias, segs = segundos2.convert_days(259201)
        self.assertEqual(3, dias)
        self.assertEqual(1, segs)

class TestHours(unittest.TestCase):

    def test_normal(self):
        horas, segs = segundos2.convert_hours(7222)
        self.assertEqual(2, horas)
        self.assertEqual(22, segs)

    def test_zero(self):
        horas, segs = segundos2.convert_hours(0)
        self.assertEqual(0, horas)
        self.assertEqual(0, segs)

    def test_exact(self):
        horas, segs = segundos2.convert_hours(7200)
        self.assertEqual(2, horas)
        self.assertEqual(0, segs)

    def test_one(self):
        horas, segs = segundos2.convert_hours(10801)
        self.assertEqual(3, horas)
        self.assertEqual(1, segs)

class TestMins(unittest.TestCase):

    def test_normal(self):
        mins, segs = segundos2.convert_minutes(122)
        self.assertEqual(2, mins)
        self.assertEqual(2, segs)

    def test_zero(self):
        mins, segs = segundos2.convert_minutes(0)
        self.assertEqual(0, mins)
        self.assertEqual(0, segs)

    def test_exact(self):
        mins, segs = segundos2.convert_minutes(180)
        self.assertEqual(3, mins)
        self.assertEqual(0, segs)

    def test_one(self):
        mins, segs = segundos2.convert_minutes(181)
        self.assertEqual(3, mins)
        self.assertEqual(1, segs)

if __name__ == '__main__':
    unittest.main()
