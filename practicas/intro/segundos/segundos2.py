#!/usr/bin/env pythoh3

'''
Program to compute days, hours, minutes, seconds from a number of seconds
This version uses some functions, and includes some tests
'''

SECONDS_IN_MIN = 60
SECONDS_IN_HOUR = 60 * SECONDS_IN_MIN
SECONDS_IN_DAY = 24 * SECONDS_IN_HOUR

def convert_days(seconds):
    """Convert seconds to maximum number of days, and remaining seconds"""
    days = seconds // SECONDS_IN_DAY
    seconds = seconds - days * SECONDS_IN_DAY
    return days, seconds

def convert_hours(seconds):
    """Convert seconds to maximum number of hours, and remaining seconds"""
    hours = seconds // SECONDS_IN_HOUR
    seconds = seconds - hours * SECONDS_IN_HOUR
    return hours, seconds

def convert_minutes(seconds):
    """Convert seconds to maximum number of minutes, and remaining seconds"""
    minutes = seconds // SECONDS_IN_MIN
    seconds = seconds - minutes * SECONDS_IN_MIN
    return minutes, seconds

if __name__ == "__main__":
    # Read total number of seconds
    tseconds = int(input("Número de segundos: "))

    # Compute days, hours, minutes and seconds
    days, seconds = convert_days(tseconds)
    hours, seconds = convert_hours(seconds)
    minutes, seconds = convert_minutes(seconds)

    # Print result
    print(f"{tseconds} segundos son {days} día(s), {hours} hora(s), {minutes} minuto(s) y {seconds} segundo(s)")

