# Estructuras de control

## Condicionales y bucles

Actividades:

* Presentación de las tres estructuras básicas de control: secuencia, condicional y bucle.

Ejercicios:

**Ejercicio:** Número mayor.
  * Ejercicio realizado en clase
  * [Enunciado](mayor/README.md)

**Ejercicio a entregar:** Números pequeños, medianos y grandes.
  * **Fecha de entrega:** 2 de octubre de 2023, 23:59
  * Entrega en foro de ejercicios de la asignatura en el aula virtual.
  * [Enunciado](numeros_clasificados/README.md).

**Ejercicio a entregar:** Animales clasificados.
  * **Fecha de entrega:** 11 de octubre de 2023, 23:59
  * Entrega en foro de ejercicios de la asignatura en el aula virtual.
  * [Enunciado](animales/README.md).


**Ejercicio:** Media de números.
  * Ejercicio realizado en clase
  * [Enunciado](media/README.md)


**Ejercicio a entregar:** Contador de contadores.
  * **Fecha de entrega:** 16 de octubre de 2023, 23:59
  * Entrega en foro de ejercicios de la asignatura en el aula virtual.
  * [Enunciado](contadores/README.md).

**Ejercicio:** "Cálculo de números primos". [Enunciado](../control/primos/README.md).
