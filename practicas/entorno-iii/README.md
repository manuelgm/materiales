# El entorno de programación III

## Entornos virtuales (venv)

Actividades:

* Explicación del entorno virtual:
  * Creación: `python3 -m venv <dir>`
  * Activación: `<dir>/bin/activate`
  * Desactivación: `deactivate`
* Entornos virtuales en PyCharm

Referencias:

* [Python: Creation of virtual environments](https://docs.python.org/3/library/venv.html)
* [PyCharm: Configure a virtual environment](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html)

## Instalación de paquetes (pip)

Actividades:

* Explicación de la instalación de paquetes:
  * https://pypi.org
  * `pip install <pkg>`

Ejercicios:

* Ejercicio: ejecuta [qr.py](../../frikiminutos-2022/qr.py)
  * Crea entorno virtual
  * Instala paquetes
  * Ejecuta `qr.py`
* Ejercicio: ejecuta [qr.py](../../frikiminutos-2022/qr.py) en PyCharm
  * Crea un entorno virtual en PyCharm
  * Instala paquetes
  * Ejecuta `qr.py`

Referencias:

* [Python: Installing Packages](https://packaging.python.org/en/latest/tutorials/installing-packages/)
* [PyCharm: Install, uninstall, and upgrade packages](https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html)


## Aserciones (assertions)

Ejemplos:

```python
num = int(input())
assert num > 10
assert num > 10, "num is not greater than 10"
assert num > 10, f"num ({num} is not greater than 10"
assert num > 10, \
  f"num ({num} is not greater than 10"
```

```commandline
assert val in ['A', 'B', 'C', 'D']
assert isinstance(num, int)
```

Referencias:

* [Python's assert: Debug and Test Your Code Like a Pro](https://realpython.com/python-assert-statement/)


## Pruebas (testing)

Actividades:

* Copia el directorio [segundos](segundos) en un repositorio local.
* Ábrelo como un proyecto con PyCharm
* Ejecuta los tests (ejecutando el directorio `tests`)
* Ejecuta los tests (ejecutando el fichero `tests/test_segundos2.py`
* Fuerza algún error, bien cambiando el código de `segundos2.py`, bien cambiando alguna de las comprobaciones en los tests.

Referencias:

* [Real Python: Getting Started With Testing in Python](https://realpython.com/python-testing/)

## Análisis estático (MyPy)

Actividades:

* Instala el plugin MyPy de PyCharm (menú `Settings`, opción `Plugins`).
* Copia el fichero [segundos/segundos.py](segundos/segundos3.py) en un directorio, y ábrelo como repositorio en PyCharm.
* Ejecuta MyPy para comprobarlo

Referencias:

* [PyCharm MyPy plugin](https://plugins.jetbrains.com/plugin/11086-mypy)
* [MyPy Documentation](https://mypy.readthedocs.io/en/stable/index.html)
* [How to start using Python Type Annotations with Mypy](https://www.stackbuilders.com/blog/using-types-in-python-with-mypy/)


Ejercicios:

**Ejercicio:** Clonación y actualización de repositorio remoto.
  * Ejercicio recomendado
  * [Enunciado](clone/README.md)

**Ejercicio a entregar:** Suma de números impares.
  * **Fecha de entrega:** 23 de octubre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/suma_impares 
  * [Enunciado](suma_impares/README.md).

Referencias:

* [Pro Git](https://git-scm.com/book/en/v2). Libro sobre git, que cubre desde el uso básico hasta varios detalles avanzados, incluyendo el uso con GitHub (y GitLab).

* [Oh My Git!](https://blinry.itch.io/oh-my-git). Juego de cartas para aprender detalles de git, incluyendo cómo entender el grafo de versiones.
