### Clonación y actualización de repositorio remoto

Clona a un directorio local el repositorio que bifurcaste (hiciste fork) del repositorio [Bifurca Repositorio](https://gitlab.eif.urjc.es/cursoprogram/bifurca-repositorio/) en un ejercicio anterior (ejercicio "Bifurcación de un repositorio"). Una vez lo hayas clonado, modifica el fichero `README.md` que hay en él, realiza una nueva versión (commit), y súbela al repositorio del que clonaste.

A continuación, crea un nuevo fichero, realiza una nueva versión que lo incluya, y súbela también al repositorio del que clonaste.

Haz lo anterior primero desde línea de comandos, y luego desde PyCharm.
