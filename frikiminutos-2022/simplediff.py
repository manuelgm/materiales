#!/usr/bin/env python3

from difflib import Differ
from pprint import pprint

text1 = """This is some text
just to have something to compare
with some other text.
""".splitlines(keepends=True)

text2 = """This is some other text
just to have something to compare
with the previous text.
""".splitlines(keepends=True)

diff = Differ()
differences = diff.compare(text1, text2)
pprint(list(differences))
