#!/usr/bin/env python3

import os

import asciipixels

text = asciipixels.image.asciify(os.path.join('misc', 'cafe-nobg.jpg'), definition=100)
print(text)
